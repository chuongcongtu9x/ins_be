import {
    Body,
    Controller,
    Delete,
    Get, NotFoundException,
    Param,
    Post,
    Put, Query,
} from '@nestjs/common';
import {UsersService} from "./users.service";

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {}

    @Get()
    async index() {
        return await this.usersService.findAll();
    }

    @Get(':id')
    async find(@Param('id') id: string) {
        return await this.usersService.findOne(id);
    }

    @Get('get-by-username/:username')
    findByUserName(@Param('username') username: string): Promise<any> {
        return this.usersService.findByUserName(username);
    }

    @Post()
    async create(@Body() createTodoDto: any) {
        console.log({ createTodoDto });
        return await this.usersService.create(createTodoDto);
    }

    @Put(':id')
    async update(@Param('id') id: string, @Body() updateTodoDto: any) {
        return await this.usersService.update(id, updateTodoDto);
    }

    @Delete(':id')
    async delete(@Param('id') id: string) {
        return await this.usersService.delete(id);
    }

    @Put('update-content/:code')
    async updateContentByCode(
        @Param('code') code: string,
        @Body('content') content: string,
    ) {
        try {
            const updatedTemplate = await this.usersService.updateContentByCode(
                code,
                content,
            );
            return { success: true, data: updatedTemplate };
        } catch (error) {
            return { success: false, message: 'Error updating content' };
        }
    }
}
